import ballerina/grpc;

public isolated client class HelloWorldClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function getMessage(GetMessageRequest|ContextGetMessageRequest req) returns (GetMessageResponse|grpc:Error) {
        map<string|string[]> headers = {};
        GetMessageRequest message;
        if (req is ContextGetMessageRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("HelloWorld/getMessage", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <GetMessageResponse>result;
    }

    isolated remote function getMessageContext(GetMessageRequest|ContextGetMessageRequest req) returns (ContextGetMessageResponse|grpc:Error) {
        map<string|string[]> headers = {};
        GetMessageRequest message;
        if (req is ContextGetMessageRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("HelloWorld/getMessage", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <GetMessageResponse>result, headers: respHeaders};
    }
}

public client class HelloWorldGetMessageResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendGetMessageResponse(GetMessageResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextGetMessageResponse(ContextGetMessageResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextGetMessageResponse record {|
    GetMessageResponse content;
    map<string|string[]> headers;
|};

public type ContextGetMessageRequest record {|
    GetMessageRequest content;
    map<string|string[]> headers;
|};

public type GetMessageResponse record {|
    string message = "";
|};

public type GetMessageRequest record {|
    string language = "";
|};

const string ROOT_DESCRIPTOR = "0A1168656C6C6F5F776F726C642E70726F746F222F0A114765744D65737361676552657175657374121A0A086C616E677561676518012001280952086C616E6775616765222E0A124765744D657373616765526573706F6E736512180A076D65737361676518012001280952076D65737361676532430A0A48656C6C6F576F726C6412350A0A6765744D65737361676512122E4765744D657373616765526571756573741A132E4765744D657373616765526573706F6E7365620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"hello_world.proto": "0A1168656C6C6F5F776F726C642E70726F746F222F0A114765744D65737361676552657175657374121A0A086C616E677561676518012001280952086C616E6775616765222E0A124765744D657373616765526573706F6E736512180A076D65737361676518012001280952076D65737361676532430A0A48656C6C6F576F726C6412350A0A6765744D65737361676512122E4765744D657373616765526571756573741A132E4765744D657373616765526573706F6E7365620670726F746F33"};
}

