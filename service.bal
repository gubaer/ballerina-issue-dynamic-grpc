import ballerina/grpc;

const map<string> MESSAGES = {
    de: "Hallo, Welt!",
    en: "Hello, World"
};

isolated service class HelloWorldService {
    *grpc:Service;

    remote function getMessage(GetMessageRequest value) returns GetMessageResponse|error {
        var language = value.language.trim().toLowerAscii();
        if ! MESSAGES.hasKey(language) {
            return error(string`unsupported language ${language}`);
        }        
        var response = {
            message: MESSAGES.get(language)
        };
        return response;
    }
}

