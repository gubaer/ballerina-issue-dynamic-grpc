import ballerina/io;
import ballerina/grpc;
import ballerina/lang.runtime;

configurable int PORT = 10001;

function runServer() returns error? {
    io:println(string `grpc_sample_generate: starting grpc service on 'localhost:${PORT}'`);

    final grpc:Listener | error result =  new (PORT);
    if result is error {
        io:println("failed to create listener - ", result);
        return result;
    }

    final grpc:Listener helloWorldListener = check result.ensureType(grpc:Listener);

    @grpc:ServiceDescriptor {
        descriptor: ROOT_DESCRIPTOR,
        descMap: getDescriptorMap()
    }
    var helloWorldService = new HelloWorldService();

    var result2 = helloWorldListener.attach(helloWorldService);
    if result2 is error {
        io:println("failed to attach listener - ", result2);
        return result2;
    }

    var result3 = helloWorldListener.start();
    if result3 is error {
        io:println("failed to start listener - ", result3);
        return result3;
    }

    io:println("before registering listener ...");
    runtime:registerListener(helloWorldListener);
}

public function main(string... args) {
    var result = runServer();
    if result is error {
        panic result;
    }
}
